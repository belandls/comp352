package Ass23;

import java.util.Iterator;

/**
 * Created by Richard and Samuel on 2014-11-19.
 *
 * The Binary Tree ADT
 *
 *
 */
public interface IBinaryTree {
    /**
    *
    * Algorithm Insert
    * Input : A node N
    * Comment : Adds a node in the index following the lastAdded value
    *
    * indexNode <- lastAdded + 1
    * set node N at indexNode
    *
     **/
    void insert(INode node);

    //returns the number of nodes. Tracked via the lastAdded variable.
    int size();

    //returns true if the node at index 1 is null, false otherwise
    boolean isEmpty();

    //returns the node at index 1
    INode root();

        /*
    * Algorithm Parent
    * Input : a node N
    * Output : a node P
    * Comment : Returns the parent of a node N.
    *
    * indexNode <- index of node N
    * if indexNode = 1 return null
    * else return node at index indexNode/2

    */
    INode parent(INode node);

        /**
    * Algorithm isInternal
    * Input : a node N
    * Output : a boolean
    * Comment : Checks if the node N is internal
    *
    * indexNode <- index of node N
    * indexLeftChild <- indexNode * 2
    * indexRightChild <- indexNode * 2 + 1
    * if nodes at indexLeftChild and indexRightChild are not both null, return true
    * else return false
    **/
    boolean isInternal(INode node);
        /**
    * Algorithm isExternal
    * Input : a node N
    * Output : a boolean
    * Comment : Checks if the node N has no children
    *
    * indexNode <- index of node N
    * indexLeftChild <- indexNode * 2
    * indexRightChild <- indexNode * 2 + 1
    * if nodes at indexLeftChild and indexRightChild are both null, return true
    * else return false
     **/
    boolean isExternal(INode node);

    /** returns the node at index 1**/
    boolean isRoot(INode node);

        /**
    * Algorithm Left
    * Input : a node N
    * Output : a node R
    * Comment : Checks if the node N as a left child and return it
    *
    * indexNode <- index of node N
    * indexLeftChild <- indexNode * 2
    * if node at indexLeftChild is not null return node at indexLeftChild
     **/
    INode left(INode node);
        /**
      * Algorithm Right
      * Input : a node N
      * Output : a node R
      * Comment : Checks if the node N as a right child and return it
      *
      * indexNode <- index of node N
      * indexRightChild <- indexNode * 2 + 1
      * if node at indexRightChild is not null return node at indexRightChild
       **/
    INode right(INode node);

        /**
      * Algorithm hasRight
      * Input : a node N
      * Output : a boolean
      * Comment : Checks if the node N as a left child
      *
      * indexNode <- index of node N
      * indexLeftChild <- indexNode * 2
      * if node at indexLeftChild is not null return true
      * else return false
       **/
    boolean hasLeft(INode node);

        /**
       * Algorithm hasRight
       * Input : a node N
       * Output : a boolean
       * Comment : Checks if the node N as a right child
       *
       * indexNode <- index of node N
       * indexRightChild <- indexNode * 2 + 1
       * if node at indexRightChild is not null return true
       * else return false
        **/
    boolean hasRight(INode node);

    /** returns the node stored at the index corresponding to the lastAdded variable**/
    INode lastNode();

        /**
        * Algorithm Replace
        * Input : Two nodes, node1 and node2
        * Output : The node that replaces the other
        * Comment : A node replaces another one at the previous' position.
        *
        * index1 <- index of node1
        * set node2 at index1
        * return node2
        *
         **/
    INode replace(INode node1, INode node2);

}
