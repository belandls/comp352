package Ass23;

import java.util.Iterator;


/**
 * Created by Samuel on 11/19/2014.
 */
public class FlexHeap implements IHeap {

    private MyBinaryTree bt;
    private HeapType type; //To know if in min or max heap

    public FlexHeap(){
        bt = new MyBinaryTree();
        type = HeapType.MIN;
    }

    public FlexHeap(HeapType t) {
        bt = new MyBinaryTree();
        this.type = t;
    }

    @Override
    public int size() {
        return bt.size();
    }

    @Override
    public boolean isEmpty() {
        return bt.isEmpty();
    }

    @Override
    public INode top() {
        return bt.root();
    }

    @Override
    public void insert(int key, char value) {
        Node temp = new Node(value, key);
        bt.insert(temp);
        adjustHeapUp();
    }


    @Override
    public INode removeTop() {
        INode root = bt.root();
        // 1 is the index if the root
        bt.replace(1, bt.lastNode());
        bt.removeLastNode();
        adjustHeapDown();
        return root;
    }

    private void adjustHeapUp(){
        if(this.type == HeapType.MIN)
        {
            this.minHeapUpheap();
        }
        else this.maxHeapUpheap();
    }

    private void adjustHeapDown(){
        if(this.type == HeapType.MIN)
        {
            this.minHeapDownheap();
        }
        else this.maxHeapDownheap();
    }

    //Heap up algorithm when in min heap
    private void minHeapUpheap() {
        int index = bt.getLastAdded();
        int parentIndex = index/2;
        while(index != 1 && bt.getNode(index).key() < bt.parent(index).key())
        {
            bt.swap(index, parentIndex);
            index = parentIndex;
            parentIndex = parentIndex/2;
        }
    }

    //Heap up algorithm when in max heap
    private void maxHeapUpheap() {
        int index = bt.getLastAdded();
        int parentIndex = index/2;
        while(index != 1 && bt.getNode(index).key() > bt.parent(index).key())
        {
            bt.swap(index, parentIndex);
            index = parentIndex;
            parentIndex = parentIndex/2;
        }

    }

    //Heap down algorithm when in min heap
    private void minHeapDownheap() {
        int index = 1;
        while (bt.hasLeft(index) || bt.hasRight(index)) {
            if (bt.hasLeft(index) && bt.hasRight(index)) {
                if (bt.getNode(index).key() > Math.min(bt.left(index).key(), bt.right(index).key())) {
                    if (bt.left(index).key() < bt.right(index).key()) {
                        bt.swap(index, index * 2);
                        index = index * 2;
                    } else {
                        bt.swap(index, index * 2 + 1);
                        index = index * 2 + 1;
                    }
                }else{
                    break;
                }
            } else if (bt.hasLeft(index)) {
                if (bt.getNode(index).key() > bt.left(index).key()) {
                    bt.swap(index, index * 2);
                    index = index * 2;
                }else{
                    break;
                }
            }
        }
    }


    //Heap down algorithm when in max heap
    private void maxHeapDownheap() {
        int index = 1;
        while (bt.hasLeft(index) || bt.hasRight(index)) {
            if (bt.hasLeft(index) && bt.hasRight(index)) {
                if (bt.getNode(index).key() < Math.max(bt.left(index).key(), bt.right(index).key())) {
                    if (bt.left(index).key() > bt.right(index).key()) {
                        bt.swap(index, index * 2);
                        index = index * 2;
                    } else {
                        bt.swap(index, index * 2 + 1);
                        index = index * 2 + 1;
                    }
                }else{
                    break;
                }
            } else if (bt.hasLeft(index)) {
                if (bt.getNode(index).key() < bt.left(index).key()) {
                    bt.swap(index, index * 2);
                    index = index * 2;
                }else{
                    break;
                }
            } else {
                if (bt.getNode(index).key() < bt.right(index).key()) {
                    bt.swap(index, index * 2 + 1);
                    index = index * 2 + 1;
                }else{
                    break;
                }
            }
        }
    }

    public void toggleHeap() {
        if(this.type == HeapType.MAX)
            this.switchMinHeap();
        else switchMaxHeap();
    }

    public void switchMinHeap() {
        if(this.type == HeapType.MAX){
            setType(HeapType.MIN);
        }
    }

    public void switchMaxHeap() {
        if(this.type == HeapType.MIN){
            setType(HeapType.MAX);
        }
    }

    private void setType(HeapType t){
        this.type = t;
        rebuildTree();
    }

    //When switching from max to min or min to max, we simply create a new heap of the opposite type, fill it up and transfer the tree back
    private void rebuildTree()
    {
        FlexHeap tempHeap = new FlexHeap(this.type);
        Iterator<INode> iter = this.bt.iterator();
        while(iter.hasNext()){
            INode n = iter.next();
            tempHeap.insert(n.key(), n.element());
        }
        this.bt = tempHeap.bt;
    }

    public void display() {
        Iterator<INode> iter = this.bt.iterator();
        while(iter.hasNext()) {
            INode n = iter.next();
            int key = n.key();
            char c = n.element();
            System.out.println("[" + key + ", " + c + "]");
        }
        System.out.println("- - - - - - - - - - - - - - - - - - ");
    }
}
