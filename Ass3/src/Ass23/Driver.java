package Ass23;

import java.util.Random;

/**
 * Created by Richard GrandMaison(6145965) and Samuel Beland-Leblanc(7185642).
 */
public class Driver {

    public static void main(String[] args){
//        MyBinaryTree testTree = new MyBinaryTree();
        FlexHeap fh = new FlexHeap();
        MyArrayList al = new MyArrayList();

        Node node100 = new Node('z', 100);
        Node node101 = new Node('!', 101);
        Node node102 = new Node('@', 102);
        Node node103 = new Node('%', 103);
        Node node104 = new Node('*', 104);
        Node node105 = new Node('7', 105);

        al.add(1, node100);
        al.add(2, node101);
        al.add(3, node104);
        al.display();
        al.remove(1);
        al.add(1, node102);
        al.display();
        System.out.println(al.size());
        al.add(4, node105);
        al.add(5, node103);
        al.display();

        MyBinaryTree bt = new MyBinaryTree();

        bt.insert(node100);
        bt.insert(node101);
        bt.insert(node102);

        System.out.println("Last Node (should be @)" + bt.lastNode().element() + " and root (should be z) : " + bt.root().element());
        System.out.println("Is Node100 the root? : " + bt.isRoot(node100));
        System.out.println("Is the Node101 the root? : " +  bt.isRoot(node101));
        System.out.println("node 101 and 102 should not have children (4 false) : " + bt.hasLeft(2) + bt.hasRight(2) + bt.hasLeft(3) + bt.hasRight(3));
        System.out.println("The parent of node101 should be the root : " + bt.isRoot(bt.parent(2)));
        bt.swap(1, 2);
        System.out.println("The node 101 should now be the root : " + bt.isRoot(node101));
        System.out.println("There is : " + bt.size() + " nodes");
        System.out.println("The left child of the root is node 101 (!) : " + bt.root().element());



        fh.insert(4,'d');
        fh.insert(3,'c');
        fh.insert(2,'b');
        fh.insert(1,'a');

        //should be 1 - 2 - 3 - 4
        fh.display();
        System.out.println();

        fh.toggleHeap();

        //should be 4 - 3 - 2 - 1
        fh.display();
        System.out.println();

        fh.switchMinHeap();

        //should be back to 1 - 2 - 3 - 4
        fh.display();
        System.out.println();

        fh.switchMaxHeap();

        //should be back to 4 - 3 - 2 - 1
        fh.display();
        System.out.println();

        fh.switchMinHeap();

        System.out.println("Removing top (should be 1): " + fh.removeTop().key());

        // 2 - 4 - 3
        fh.display();
        System.out.println();

        fh.switchMaxHeap();

        // 4 - 2 - 3
        fh.display();
        System.out.println();

        System.out.println("Removing top after toggle (should be 4): " + fh.removeTop().key());

        System.out.println("Checking top (should be 3): " + fh.top().key());


        fh.insert(7,'g');
        fh.insert(6,'f');
        fh.insert(3,'c');
        fh.insert(8,'h');
        fh.display();
        System.out.println(fh.top().key());
        fh.toggleHeap();
        System.out.println(fh.top().key());
        fh.display();
        fh.removeTop();
        System.out.println(fh.top().key());
        fh.display();
        fh.removeTop();
        fh.display();
        fh.removeTop();
        fh.display();
        fh.removeTop();
        fh.display();
        fh.removeTop();
        fh.display();

        /*System.out.println(fh.removeTop().element());
        System.out.println(fh.removeTop().element());
        fh.toggleHeap();
        System.out.println(fh.removeTop().element());
        System.out.println(fh.removeTop().element());
        */


        Random rnd = new Random();



       for (int i = 0; i < 100; i++){
            fh.insert(i, (char)rnd.nextInt());
        }

        fh.switchMinHeap();
        while(fh.top() != null){
            System.out.println(fh.removeTop().key());
        }

        for (int i = 0; i < 10000; i++){
            fh.insert(i, (char)rnd.nextInt());
        }

        //alternating while removing in a large heap
        while(fh.top() != null){
            INode temp = fh.removeTop();
            System.out.println(temp.key());
            if (temp.key() % 1000 == 0 ){
                fh.toggleHeap();
            }
        }

    }

}
