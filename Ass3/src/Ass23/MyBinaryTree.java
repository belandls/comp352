package Ass23;

import java.util.Iterator;

/**
 * Created by Richard on 2014-11-19.
 */
public class MyBinaryTree implements IBinaryTree, Iterable<INode> {

    private MyArrayList arrayList;
    private int lastAdded;

    public MyBinaryTree() {
        this.arrayList = new MyArrayList();
        lastAdded = 0;

    }

    @Override
    public void insert(INode node) {
        this.arrayList.add(this.lastAdded + 1, node);
        this.lastAdded++;
    }

    public int getLastAdded() { return this.lastAdded; }

    public INode getNode(int i) {
        return this.arrayList.get(i);
    }

    public void removeLastNode(){
        this.arrayList.remove(this.lastAdded--);
    }

    public void decrementLastAdded(){
        this.lastAdded--;
    }

    @Override
    public int size() {
        return this.lastAdded;
    }

    @Override
    public boolean isEmpty() {
        return this.arrayList.isEmpty();
    }

    @Override
    public INode root() {
        try{
            return this.arrayList.get(1);
        }catch (Exception ex){
            return null;
        }
    }

    @Override
    public INode parent(INode node) {
        int index = this.arrayList.indexOf(node);
        if (index > 1){
            int parent = -1;
            if (index % 2 == 0){
                parent = index / 2;
            }else{
                parent = (int)Math.floor(index/2);
            }
            return this.arrayList.get(parent);
        }
        return null;
    }

    public INode parent(int i) {
        if(i > 1){
            int parent = i/2;
            return this.arrayList.get(parent);
        }
        return null;
    }

    @Override
    public boolean isInternal(INode node) {
        int index = this.arrayList.indexOf(node);
        if(left(index) != null || right(index) != null)
            return true;
        return false;
    }

    @Override
    public boolean isExternal(INode node) {
        int index = this.arrayList.indexOf(node);
        if(left(index) == null && right(index) == null)
            return true;
        return false;
    }

    @Override
    public boolean isRoot(INode node) {
        return this.arrayList.get(1) == node;
    }

    @Override
    public INode left(INode node) {
        int index = this.arrayList.indexOf(node);
        try{
            return this.arrayList.get(index*2);
        }catch(Exception ex){
            return null;
        }
    }

    public INode left(int i) {
        try{
            return this.arrayList.get(i*2);
        }catch(Exception ex){
            return null;
        }
    }

    @Override
    public INode right(INode node) {
        int index = this.arrayList.indexOf(node);
        try{
            return this.arrayList.get(index*2 + 1);
        }catch(Exception ex){
            return null;
        }
    }

    public INode right(int i) {
        try{
            return this.arrayList.get(i*2 + 1);
        }catch(Exception ex){
            return null;
        }
    }

    @Override
    public boolean hasLeft(INode node) {
        int index = this.arrayList.indexOf(node);
        try{
            return this.arrayList.get(index*2) != null;
        }catch(Exception ex){
            return false;
        }
    }

    public boolean hasLeft(int i) {
        try{
            return this.arrayList.get(i*2) != null;
        }catch(Exception ex){
            return false;
        }
    }

    @Override
    public boolean hasRight(INode node) {
        int index = this.arrayList.indexOf(node);
        try{
            return this.arrayList.get(index*2 + 1) != null;
        }catch(Exception ex){
            return false;
        }
    }

    public boolean hasRight(int i) {
        try{
            return this.arrayList.get(i*2 + 1) != null;
        }catch(Exception ex){
            return false;
        }
    }

    @Override
    public INode lastNode() {
        return this.arrayList.get(this.lastAdded);
    }

    @Override
    public INode replace(INode node1, INode node2) {
        int index = this.arrayList.indexOf(node1);
        this.arrayList.set(index, node2);
        return node2;
    }

    public INode replace(int i1, INode node2) {
        this.arrayList.set(i1, node2);
        return node2;
    }
    public void swap(INode node1, INode node2) {
        int index1 = this.arrayList.indexOf(node1);
        int index2 = this.arrayList.indexOf(node2);
        this.arrayList.set(index1, node2);
        this.arrayList.set(index2, node1);
    }

    public void swap(int i1, int i2) {
        INode node1 = this.arrayList.get(i1);
        INode node2 = this.arrayList.get(i2);
        this.arrayList.set(i1, node2);
        this.arrayList.set(i2, node1);

    }


    @Override
    public Iterator<INode> iterator() {
        return this.arrayList.iterator();
    }
}
