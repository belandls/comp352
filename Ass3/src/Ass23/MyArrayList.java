package Ass23;

import java.util.Iterator;

/**
 * Created by Richard on 2014-11-19.
 */
public class MyArrayList implements IArrayList, Iterable<INode> {

    static private final int INITIAL_SIZE = 8;

    private INode[] nodes;
    private int currentSize;

    public MyArrayList(){
        nodes = new Node[INITIAL_SIZE];
        this.currentSize = INITIAL_SIZE;
    }

    @Override
    public INode get(int i) {
        if (i < 0 || i >= this.currentSize){
            throw new IndexOutOfBoundsException();
        }else {
            return this.nodes[i];
        }
    }

    @Override
    public void set(int i, INode node) {
        if (i < 0 || i >= this.currentSize){
            throw new IndexOutOfBoundsException();
        }else {
            this.nodes[i] = node;
        }
    }

    @Override
    public void add(int i, INode node) {
        if(i >= this.currentSize || i < 0) {
            throw new IndexOutOfBoundsException();
        } else {
            calculateNewSizeFromIndex(i);
            this.nodes[i] = node;
        }
    }

    @Override
    public char remove(int i) {
        if (i < 0 || i >= this.currentSize){
            throw new IndexOutOfBoundsException();
        } else {
            char temp = this.nodes[i].element();
            this.nodes[i]= null;
            return temp;
        }
    }

    @Override
    public int size() {
        return this.nodes.length;
    }

    @Override
    public boolean isEmpty() {
        return (this.nodes[1].element() == '\u0000');
    }

    public int indexOf(INode n){
        for (int i = 0; i < this.currentSize; i++){
            if (this.nodes[i] == n){
                return i;
            }
        }
        return -1;
    }


    private void calculateNewSizeFromIndex(int index){
        while(index >= this.currentSize - 1){
            expandArray();
        }

    }

    private void expandArray(){
        INode[] temp = new Node[this.currentSize * 2];
        for (int i = 0 ; i < this.currentSize; i++){
            temp[i] = this.nodes[i];
        }
        this.nodes = temp;
        this.currentSize *= 2;

    }

    public void display() {
        System.out.print("[");
        for(int i = 0; i < this.size() - 1; i++)
        {
            if(this.nodes[i] != null)
                System.out.print("(" + nodes[i].key() + ", " + nodes[i].element() + ") ");
        }
        System.out.println("]\n");
    }

    @Override
    public Iterator<INode> iterator() {
        return new ArrayListIterator(this.nodes);
    }
}
