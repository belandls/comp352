package Ass23;
/**
 * Created by Richard and Samuel on November 19th 2014.
 */
public interface IArrayList {

    INode get(int i);

    void set(int i, INode node);

    void add(int i, INode node);

    char remove(int i);

    int size();

    boolean isEmpty();

}
