package Ass23;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator made for the binary tree. It's not generic since it starts at index 1 which is our root.
 */
public class ArrayListIterator implements Iterator<INode> {

    private INode[] nodes;
    private int index;


    public ArrayListIterator(INode[] nodes){
        this.nodes = new INode[nodes.length];
        for (int i = 0 ; i < nodes.length ; i++){
            this.nodes[i] = nodes[i];
        }
        this.index = 1;
    }

    @Override
    public boolean hasNext() {
        return (!(index >= this.nodes.length) && !(this.nodes[index] == null));
    }

    @Override
    public INode next() {
        if(hasNext()){
            return this.nodes[index++];
        }else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public void remove() {
        throw new NotImplementedException();
    }
}
