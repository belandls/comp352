package Ass23;

/**
 * Created by Richard on 2014-11-19.
 */
public class Node implements INode {

    private char element;
    private int key;

    public Node(char c, int key){
        this.element = c;
        this.key = key;
    }

    @Override
    public char element() {
        return this.element;
    }

    @Override
    public int key() {
        return this.key;
    }


    @Override
    public void setElement(char c) {
        this.element = c;
    }

    @Override
    public void setKey(int key) {
        this.key = key;
    }

}
