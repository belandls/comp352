package Ass23;

/**
 *
 * Heap ADT
 *
 * The heap will use a binary tree internally to represents it's data.
 * the basic operations of the heap (insert, removeTop, up-heap, down-heap, etc.) will work on the binary tree itself, swapping nodes and removing either the top one or the last one and also by adding at the end of the tree.
 *
 *
 * */
public interface IHeap {

    //Returns the current number of element
    int size();

    boolean isEmpty();

    INode top();


    /**
    *
    * Algorithm Insert
    * Input : A key K and an value V
    * Output : Nothing
    * Comment : This algorithm is similar for the max heap. The key validations are simply backwards
    *
    * N <- new Node with K and V
    * BT.insert(N)
    *
    * insertedNodeIndex <- BT.LastNodeIndex
    * parentNodeIndex <- insertedNodeIndex / 2
    *
    * While insertedNodeIndex != BT.root AND BT.Node(insertedNodeIndex).Key < BT.Parent(insertedNodeIndex).Key
    *
    *   BT.Swap(insertedNodeIndex, parentNodeIndex)
    *   insertedNodeIndex <- parentNodeIndex
    *   parentNodeIndex <- parentNodeIndex / 2
    *
    * End While
    *
     **/
    void insert(int key, char value);

    /**
    *
    * Algorithm removeTop
    * Input : Nothing
    * Output : The top node of the Heap.
    * Comment : This algorithm is similar for the max heap. The key validations are simply backwards
    *
    * rootNode <- BT.root
    * BT.replace(root, BT.LastNode)
    * BT.removeLastNode()
    *
    * currentNode <- rootNode
    *
    * While currentNode.hasLeft Or currentNode.hasRight
    *
    *   If currentNode.hasLeft AND currentNode.hasRight Then
    *       If currentNode.Key > Min(currentNode.left.Key, currentNode.right.Key) Then
    *           If currentNode.left.key < currentNode.right.key then
    *               BT.swap(currentNode, currentNode.left)
    *               currentNode <- currentNode.left
    *           Else
    *               BT.swap(currentNode, currentNode.right)
    *               currentNode <- currentNode.right
    *           End If
    *       Else
    *           Break
    *       End If
    *
    *   Else If currentNode.hasLeft
    *       If currentNode.Key > currentNode.left.Key  Then
    *           BT.swap(currentNode, currentNode.left)
    *           currentNode <- currentNode.left
    *       Else
    *           Break
    *       End IF
    *   End if
    *
    * End While
    *
    * Return rootNode
    *
     **/
    INode removeTop();

}
